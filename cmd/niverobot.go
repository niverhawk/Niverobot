package main

import (
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"log"
	"niverobot/database"
	"niverobot/gym"
	"niverobot/helloWorld"
	"niverobot/model"
	"niverobot/timeUntil"
	"niverobot/user"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println(".env file not found... loading vars from environment")
	}
	var botToken = os.Getenv("BOT_API_TOKEN")
	if botToken == "" {
		log.Panic("cannot find env variable BOT_API_TOKEN")
	}

	db, err := database.NewDatabase()
	if err != nil {
		log.Panic(err)
	}

	telegramBot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		log.Panic("err init tgbotapi", err)
	}
	telegramBot.Debug = true

	chatBot := model.ChatBot{
		Db:  db,
		Api: telegramBot,
	}

	config := tgbotapi.NewUpdate(0)
	config.Timeout = 60

	updates := telegramBot.GetUpdatesChan(config)

	// Make sure help utils are init first
	actions := []model.Action{
		user.NewManager(),
		helloWorld.NewHelloWorld(),
		timeUntil.NewHelp(),
		timeUntil.NewFormatsHelp(),
		timeUntil.NewTimeUntil(),
		gym.NewGymGoal(),
		gym.NewGym(),
	}

	for update := range updates {
		//Loop through all feature concurrently
		for _, i := range actions {
			if trigger, stopProcessing := i.TriggerAndStopProcessing(update); trigger {
				go i.Execute(update, &chatBot)
				if stopProcessing {
					break
				}
			}
		}
	}
}
