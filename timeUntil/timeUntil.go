package timeUntil

import (
	"errors"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"niverobot/model"
	"regexp"
	"strings"
	"time"
)

var TimeNotParsed = errors.New("Dries stop met mn bot kapot te maken")
var TooManyArguments = errors.New("Too many arguments")
var IncorrectArguments = errors.New("Incorrect arguments")

type TimeUntil struct{}

func NewTimeUntil() *TimeUntil {
	return &TimeUntil{}
}

func (s *TimeUntil) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	var output string
	var err error
	removeCommand := strings.SplitAfter(update.Message.Text, ".tu ")
	if len(removeCommand) > 1 {
		input := strings.Split(removeCommand[1], " ")
		switch len(input) {
		case 1, 2, 3:
			matched := regexp.MustCompile(`-\b[yMdhms]\b`).MatchString(input[0])
			if matched {
				output, err = getFormattedTimeUntilMessage(input[0], strings.Join(input[1:], " "))
			} else {
				output, err = getFormattedTimeUntilMessage("", strings.Join(input, " "))
			}
		default:
			err = TooManyArguments
		}
	} else {
		err = IncorrectArguments
	}

	var message tgbotapi.MessageConfig
	if err != nil {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, err.Error())
	} else {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, output)
	}
	chatBot.Api.Send(message)
}

func (s *TimeUntil) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	return update.Message != nil && update.Message.Text != "" &&
		strings.Index(update.Message.Text, ".tu") == 0, true
}

func getFormattedTimeUntilMessage(truncateFormatting string, input string) (string, error) {
	if duration, showSeconds, err := calcDurationDiff(time.Now(), input); err != nil {
		return "", err
	} else {
		formattedDuration := formatDuration(duration, truncateFormatting, showSeconds)
		return fmt.Sprintf("%s until %s", formattedDuration, input), nil
	}
}

// calcDurationDiff function that calculates duration
// if seconds are included in the end time it will show the seconds.
func calcDurationDiff(start time.Time, end string) (time.Duration, bool, error) {
	var parsedEndTime time.Time

	for _, format := range formats {
		resp, err := time.Parse(format, end)
		if err == nil {
			parsedEndTime = resp
			switch {
			// Only time was provided, add the current date
			case resp.Year() == 0 && resp.Month() == 1 && resp.Day() == 1:
				parsedEndTime = time.Date(start.Year(), start.Month(), start.Day(), resp.Hour(), resp.Minute(), resp.Second(), resp.Nanosecond(), start.Location())
			// Only day-month was provided, add the current year
			case resp.Year() == 0 && resp.Month() != 0 && resp.Day() != 0:
				parsedEndTime = time.Date(start.Year(), resp.Month(), resp.Day(), 0, 0, 0, 0, start.Location())
				fallthrough
			case resp.Year() == 0 && resp.Month() != 0 && resp.Day() != 0 && resp.Hour() != 0:
				parsedEndTime = parsedEndTime.Add(time.Duration(resp.Hour()) * time.Hour)
				fallthrough
			case resp.Year() == 0 && resp.Month() != 0 && resp.Day() != 0 && resp.Minute() != 0:
				parsedEndTime = parsedEndTime.Add(time.Duration(resp.Minute()) * time.Minute)
				fallthrough
			case resp.Year() == 0 && resp.Month() != 0 && resp.Day() != 0 && resp.Second() != 0:
				parsedEndTime = parsedEndTime.Add(time.Duration(resp.Second()) * time.Second)
			// If a two-digit year was provided, handle it (assuming it's 20xx)
			case resp.Year() < 100:
				parsedEndTime = time.Date(2000+resp.Year(), resp.Month(), resp.Day(), resp.Hour(), resp.Minute(), resp.Second(), resp.Nanosecond(), start.Location())
			}
			break
		}
	}
	location, err := time.LoadLocation("Europe/Amsterdam")
	if err != nil {
		fmt.Println("Error loading location:", err)
	}

	if parsedEndTime.Year() > 2 {
		return parsedEndTime.In(location).Sub(start.In(location)), parsedEndTime.In(location).Second() > 0, nil
	}

	return -1 * time.Nanosecond, false, TimeNotParsed
}

func formatDuration(duration time.Duration, startSetting string, includeSeconds bool) string {
	var sb strings.Builder
	switch startSetting {
	// If empty then do full formatting
	case "", "-y":
		years := int(duration.Hours() / 24 / 365)
		duration -= time.Duration(years) * 365 * 24 * time.Hour
		if years != 0 {
			sb.WriteString(fmt.Sprintf("%d years ", years))
		}
		fallthrough
	case "-M":
		months := int(duration.Hours() / 24 / 30)
		duration -= time.Duration(months) * 30 * 24 * time.Hour
		if months != 0 {
			sb.WriteString(fmt.Sprintf("%d months ", months))
		}
		fallthrough
	case "-d":
		days := int(duration.Hours() / 24)
		duration -= time.Duration(days) * 24 * time.Hour
		if days != 0 {
			sb.WriteString(fmt.Sprintf("%d days ", days))
		}
		fallthrough
	case "-h":
		hours := int(duration.Hours())
		duration -= time.Duration(hours) * time.Hour
		if hours != 0 {
			sb.WriteString(fmt.Sprintf("%d hours ", hours))
		}
		fallthrough
	case "-m":
		minutes := int(duration.Minutes())
		duration -= time.Duration(minutes) * time.Minute
		if minutes != 0 {
			sb.WriteString(fmt.Sprintf("%d minutes ", minutes))
		}
		fallthrough
	case "-s":
		// Seconds is always an exception because feature
		if startSetting == "-s" || includeSeconds {
			sb.WriteString(fmt.Sprintf("%d seconds", int(duration.Seconds())))
		}
	}

	return sb.String()
}
