package timeUntil

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"niverobot/model"
	"strings"
)

var formats = []string{
	"Monday, 02 January 2006 15:04:05",
	"02/Jan/2006:15:04:05 -0700",
	"Monday, 02-Jan-06 15:04:05",
	"Mon, 02 Jan 2006 15:04:05",
	"January 02, 2006 15:04:05",
	"2006-01-02T15:04:05Z07:00",
	"02 January 2006 15:04:05",
	"Mon, 02-Jan-06 15:04:05",
	"Jan 02, 2006 15:04:05",
	"02 Jan 2006 15:04:05",
	"2006-01-02T15:04:05Z",
	"02-Jan-2006 15:04:05",
	"2006-01-02 15:04:05",
	"Jan-02-06 15:04:05",
	"02-01-2006 15:04:05",
	"02-Jan-06 15:04:05",
	"02-Jan-2006 15:05",
	"02-01-2006 15:05",
	"2006-01-02 15:05",
	"2006-01-02",
	"02-01-06",
	"02-01-2006",
	"2-01-2006",
	"02-01 15:04",
	"15:04:05",
	"2-1-2006",
	"15:04",
	"02-01",
	"2-01",
	"2-1",
}

type SupportedFormats struct{}

func NewFormatsHelp() *SupportedFormats {
	return &SupportedFormats{}
}

func (t *SupportedFormats) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	var sb strings.Builder
	sb.WriteString(supportedFormatsMessage())
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, sb.String())
	chatBot.Api.Send(msg)
}

func (t *SupportedFormats) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	return update.Message != nil && update.Message.Text != "" &&
		strings.Contains(update.Message.Text, ".tu -f"), true
}

func supportedFormatsMessage() string {
	supportedFormats := strings.Join(formats, "\n")
	return fmt.Sprintf("Supported formats are: \n%s", supportedFormats)
}
