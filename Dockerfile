# syntax=docker/dockerfile:1

FROM golang:1.23.0-alpine
WORKDIR /app

COPY . .

RUN go mod download
RUN go mod verify

RUN go build -o niverobot ./cmd

CMD [ "/app/niverobot" ]