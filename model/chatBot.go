package model

import (
	"github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gorm.io/gorm"
)

type ChatBot struct {
	Db  *gorm.DB
	Api *tgbotapi.BotAPI
}
