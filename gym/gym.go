package gym

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gorm.io/gorm"
	"niverobot/model"
	"niverobot/user"
	"regexp"
	"strings"
	"time"
)

const (
	triggerCommand = ".sport"
)

type Gym struct{}

type Stats struct {
	gorm.Model
	UserID   int64
	Exercise string
}

func NewGym() *Gym {
	return &Gym{}
}

func (s *Gym) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	exercise := parseExercise(update.Message.Text)

	// Store new entry
	userId := update.Message.From.ID
	result := chatBot.Db.Create(&Stats{UserID: userId, Exercise: exercise})
	var message tgbotapi.MessageConfig
	if result.Error != nil {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, result.Error.Error())
		chatBot.Api.Send(message)
		return
	}

	// Retrieve this years count and send message
	goalCount, err := getGoalCount(chatBot.Db, userId)
	if err != nil {
		goalCount = 0
	}
	startOfWeek := getStartOfWeek(time.Now())

	var curCount int64
	if err := chatBot.Db.Model(&Stats{}).
		Where(&Stats{UserID: userId}).
		Where("created_at >= ? ", startOfWeek).
		Count(&curCount).Error; err != nil {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, err.Error())
	} else {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Activity succesfully registered, %s has logged %d out of %d activities this week", update.Message.From.UserName, curCount, goalCount))
	}
	chatBot.Api.Send(message)
}

func getStartOfWeek(curTime time.Time) time.Time {
	weekday := int(curTime.Weekday())

	// If today is Sunday (0), subtract 6 days to get last Monday.
	// Otherwise, subtract (weekday - 1) days to get Monday.
	if weekday == 0 {
		weekday = 7
	}
	start := curTime.AddDate(0, 0, -(weekday - 1)) // Move back to Monday

	// Reset to 00:00:00 (midnight)
	start = time.Date(start.Year(), start.Month(), start.Day(), 0, 0, 0, 0, start.Location())

	return start
}

func getGoalCount(db *gorm.DB, userId int64) (int64, error) {
	var curUser user.User
	result := db.Model(&user.User{}).Where(&user.User{ID: userId}).First(&curUser)
	if result.Error != nil {
		return 0, result.Error
	}
	return curUser.GymGoal, nil
}

func parseExercise(message string) string {
	reason := strings.Split(message, " ")
	if len(reason) == 1 {
		return "gym"
	} else {
		return reason[1]
	}
}

func (s *Gym) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	if update.Message == nil || update.Message.Text == "" {
		return false, false
	}
	pattern := regexp.MustCompile(`^\.sports?\b`)
	return pattern.MatchString(update.Message.Text), true
}
