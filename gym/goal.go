package gym

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"niverobot/model"
	"niverobot/user"
	"regexp"
	"strconv"
	"strings"
)

type Goal struct{}

func NewGymGoal() *Goal {
	return &Goal{}
}

func (s *Goal) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	goalPerWeek, err := parseGoal(update.Message.Text)
	var message tgbotapi.MessageConfig
	if err != nil {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, err.Error())
		chatBot.Api.Send(message)
		return
	}

	result := chatBot.Db.Model(&user.User{}).Where(&user.User{ID: update.Message.From.ID}).Update("gym_goal", goalPerWeek)
	if result.Error != nil {
		message = tgbotapi.NewMessage(update.Message.Chat.ID, result.Error.Error())
		chatBot.Api.Send(message)
		return
	}

	// Retrieve this years count and send message
	message = tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Updated weekly gym goal to %d", goalPerWeek))
	chatBot.Api.Send(message)
}

func parseGoal(message string) (int, error) {
	items := strings.Split(message, " ")
	if len(items) != 3 {
		return 0, fmt.Errorf("please enter a number e.g. \".sports -g 1\"")
	}
	valueFromMessage, err := strconv.Atoi(items[2])
	if err != nil {
		return 0, fmt.Errorf("err converting value to int")
	}
	return valueFromMessage, nil
}

func (s *Goal) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	if update.Message == nil || update.Message.Text == "" {
		return false, false
	}
	pattern := regexp.MustCompile(`^\.sports?\s-g\s\d+`)
	return pattern.MatchString(update.Message.Text), true
}
