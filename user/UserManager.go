package user

import (
	"errors"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gorm.io/gorm"
	"niverobot/model"
)

type Manager struct{}

func NewManager() *Manager {
	return &Manager{}
}

func (t *Manager) Execute(update tgbotapi.Update, chatBot *model.ChatBot) {
	var user User
	result := chatBot.Db.Where(&User{ID: update.Message.From.ID}).First(&user)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		user = User{
			ID:           update.Message.From.ID,
			FirstName:    &update.Message.From.FirstName,
			LastName:     &update.Message.From.LastName,
			Username:     &update.Message.From.UserName,
			LanguageCode: &update.Message.From.LanguageCode,
			IsBot:        update.Message.From.IsBot,
		}
		chatBot.Db.Create(&user)
	}
}

func (t *Manager) TriggerAndStopProcessing(update tgbotapi.Update) (bool, bool) {
	return true, false
}
